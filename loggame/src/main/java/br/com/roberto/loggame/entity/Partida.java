package br.com.roberto.loggame.entity;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import br.com.roberto.loggame.exception.RequiredFieldException;

public class Partida {

	private Integer codigo;
	private Map<String, Jogador> assassinatos = new HashMap<String, Jogador>();
	private Integer totalMortes = 0;

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public Collection<Jogador> getAssassinatos() {
		return Collections.unmodifiableCollection(assassinatos.values());
	}

	public Integer getTotalMortes() {
		return totalMortes;
	}

	public void addAssassinato(String nomeAssassino, String nomeAssassinado) throws RequiredFieldException {
		Jogador assassino = recuperaJogador(nomeAssassino);
		Jogador assassinado = recuperaJogador(nomeAssassinado);

		assassino.addMorte();
		assassinado.addAssassinato();
	}

	private Jogador recuperaJogador(String nomeJogador) throws RequiredFieldException {
		Jogador jogador = assassinatos.get(nomeJogador);

		if (jogador == null) {
			jogador = new Jogador(nomeJogador);
			assassinatos.put(nomeJogador, jogador);
		}
		
		return jogador;
	}
	
}
