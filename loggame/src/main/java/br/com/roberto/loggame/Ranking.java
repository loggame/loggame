package br.com.roberto.loggame;

import java.util.Arrays;
import java.util.Collection;

import br.com.roberto.loggame.entity.Jogador;
import br.com.roberto.loggame.entity.Partida;

public class Ranking {

	private static Ranking instance;

	public static Ranking getInstance() {
		if (instance == null) {
			instance = new Ranking();
		}

		return instance;
	}

	private Ranking() {

	}

	public Jogador[] montarRanking(Partida partida) {
		Collection<Jogador> lista = partida.getAssassinatos();
		Jogador[] jogadores = new Jogador[lista.size()];
		partida.getAssassinatos().toArray(jogadores);
		Arrays.sort(jogadores);

		return jogadores;
	}

	
}
