package br.com.roberto.loggame;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import br.com.roberto.loggame.entity.Partida;
import br.com.roberto.loggame.exception.RequiredFieldException;

public class LogReader {

	private static LogReader instance;

	public static LogReader getInstance() {
		if (instance == null) {
			instance = new LogReader();
		}

		return instance;
	}

	private LogReader() {
	}

	/**
	 * Método responsável por ler as linhas do arquivo e identificar as partidas
	 * e assassinatos ocorridos em cada partida
	 * 
	 * @param nomeArquivo
	 * @return
	 * @throws IOException
	 * @throws RequiredFieldException
	 */
	public List<Partida> lerArquivo(String nomeArquivo) throws IOException, RequiredFieldException {
		File arquivo = new File(nomeArquivo);
		if (!arquivo.exists()) {
			return null;
		}

		List<Partida> partidas = new ArrayList<Partida>();

		BufferedReader bufferedReader = new BufferedReader(new FileReader(arquivo));
		Partida partidaAtual = null;

		while (true) {
			String linha = bufferedReader.readLine();

			if (linha == null) {
				break;
			}

			if (partidaAtual == null) {
				partidaAtual = decodificarPartida(linha);
				partidas.add(partidaAtual);
			} else {
				partidaAtual = processarLinha(partidaAtual, linha);
			}
		}

		bufferedReader.close();

		return partidas;
	}

	private Partida processarLinha(Partida partida, String linha) throws RequiredFieldException {
		if (linha.matches(".* - Match.*")) {
			return null;
		} else if (linha.matches(".* killed .* using .*")) {
			Pattern pattern = Pattern.compile(".* - (.*) killed (.*) using .*");
			Matcher matcher = pattern.matcher(linha);

			matcher.find();

			partida.addAssassinato(matcher.group(2), matcher.group(1));
		}

		return partida;
	}

	private Partida decodificarPartida(String linha) {
		Partida partida = new Partida();

		if (linha.matches(".*New match.*")) {
			Pattern pattern = Pattern.compile("(.*) - New match (\\d+) has started");
			Matcher matcher = pattern.matcher(linha);
			matcher.find();
			partida.setCodigo(Integer.valueOf(matcher.group(2)));
		} else {
			throw new RuntimeException("linha invalida");
		}

		return partida;
	}

}
