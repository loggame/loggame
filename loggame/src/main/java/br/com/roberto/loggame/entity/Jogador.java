package br.com.roberto.loggame.entity;

import br.com.roberto.loggame.exception.RequiredFieldException;

public class Jogador implements Comparable<Jogador> {

	private String nome;
	private Integer qtdeMorte = 0;
	private Integer qtdeAssassinato = 0;
	private Integer maiorSequencia = 0;
	private Integer sequenciaAtual = 0;

	public Jogador(String nome) throws RequiredFieldException {
		if (nome == null || nome.isEmpty()) {
			throw new RequiredFieldException("nome");
		}

		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void addMorte() {
		this.qtdeMorte++;
		this.maiorSequencia = Integer.max(this.sequenciaAtual, this.maiorSequencia);
		this.sequenciaAtual = 0;
	}

	public void addAssassinato() {
		this.qtdeAssassinato++;
		this.sequenciaAtual++;
	}

	
	
	public Integer getQtdeMorte() {
		return qtdeMorte;
	}

	public Integer getQtdeAssassinato() {
		return qtdeAssassinato;
	}

	public Integer getMaiorSequencia() {
		this.maiorSequencia = Integer.max(this.sequenciaAtual, this.maiorSequencia);
		return maiorSequencia;
	}

	@Override
	public int hashCode() {
		return getNome().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Jogador) {
			return ((Jogador) obj).getNome().equals(getNome());
		} else {
			return false;
		}
	}

	public Double taxa() {
		return ((double) qtdeMorte) / (qtdeMorte + qtdeAssassinato);
	}

	public int compareTo(Jogador o) {
		int comparacao = this.taxa().compareTo(o.taxa());

		// Caso a taxa de morte seja igual, verifica se há diferenças nos
		// parâmetros que compõem a taxa, verificando a taxa de assassinato e,
		// se necessário, a taxa de morte dos jogadores envolvidos.
		if (comparacao == 0) {
			comparacao = o.qtdeAssassinato.compareTo(this.qtdeAssassinato);

			if (comparacao == 0) {
				comparacao = this.qtdeMorte.compareTo(o.qtdeMorte);
			}
		}

		return comparacao;
	}

}
