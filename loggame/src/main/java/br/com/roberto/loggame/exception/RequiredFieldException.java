package br.com.roberto.loggame.exception;

public class RequiredFieldException extends Exception {

	private static final long serialVersionUID = 5087609181559481585L;

	public RequiredFieldException(String fieldName) {
		super("O campo " + fieldName + " não pode ser nulo.");
	}

}
