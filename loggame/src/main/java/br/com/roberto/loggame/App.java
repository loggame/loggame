package br.com.roberto.loggame;

import java.io.IOException;
import java.util.List;

import br.com.roberto.loggame.entity.Jogador;
import br.com.roberto.loggame.entity.Partida;
import br.com.roberto.loggame.exception.RequiredFieldException;

/**
 * Hello world!
 *
 */
public class App {

	public static void main(String[] args) throws IOException, RequiredFieldException {
		if (args == null || args.length == 0) {
			System.out.println("Você precisa informar a localização do arquivo para a elaboração do ranking.");
		} else {
			List<Partida> partidas = LogReader.getInstance().lerArquivo(args[0]);

			for (Partida partida : partidas) {
				System.out.println("Ranking da partida " + partida.getCodigo());
				Jogador[] ranking = Ranking.getInstance().montarRanking(partida);
				mostrarRanking(ranking);
			}
		}
	}

	private static void mostrarRanking(Jogador[] ranking) {
		System.out.print(completaTamanho("Nome Jogador", 30));
		System.out.println(" | Matou | Morreu | Maior Sequência");
		
		for (Jogador jogador : ranking) {
			System.out.println(montarSaida((Jogador) jogador));
		}
	}

	private static String completaTamanho(String valor, int tamanho) {
		while (valor.length() < tamanho) {
			valor = valor + " ";
		}

		return valor;
	}

	private static String montarSaida(Jogador jogador) {
		StringBuilder builder = new StringBuilder();

		builder.append(completaTamanho(jogador.getNome(), 30));
		builder.append(" |     ");
		builder.append(jogador.getQtdeAssassinato());
		builder.append(" |      ");
		builder.append(jogador.getQtdeMorte());
		builder.append(" |     ");
		builder.append(jogador.getMaiorSequencia());

		return builder.toString();
	}

}
