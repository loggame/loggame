package br.com.roberto.loggame;

import java.util.List;

import br.com.roberto.loggame.entity.Jogador;
import br.com.roberto.loggame.entity.Partida;
import junit.framework.TestCase;

public class LogReaderTest extends TestCase {

	public void testArquivoInexistente() throws Exception {
		assertNull(LogReader.getInstance().lerArquivo("C:/arquivoinexistente.txt"));
	}

	public void testArquivoExistente() throws Exception {
		assertNotNull(LogReader.getInstance().lerArquivo("C:/teste.log"));
	}

	public void testLeituraDados() throws Exception {
		List<Partida> partidas = LogReader.getInstance().lerArquivo("C:/teste.log");
		Jogador[] ranking = Ranking.getInstance().montarRanking(partidas.get(0));

		assertNotNull(partidas);
		assertFalse(partidas.isEmpty());
		assertEquals(1, partidas.size());
		assertNotNull(partidas.get(0).getCodigo());
		assertNotNull(partidas.get(0).getAssassinatos());
		assertNotNull(ranking);
		assertEquals(2, ranking.length);
	}

}
